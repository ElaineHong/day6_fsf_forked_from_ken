var express = require("express");

var app = express();
var bodyParser = require("body-parser");

app.use(bodyParser.urlencoded({'extended':'true'}));
app.use(bodyParser.json());

app.use(express.static(__dirname + "/public"));
app.use(express.static(__dirname + "/public/bower_components"));

app.post("/register", function(req, res){
    var firstName = req.body.params.firstName;
    var lastName = req.body.params.lastName;
    var postalCode = req.body.params.postalCode;
    var email = req.body.params.email;
    console.info("First Name : %s", firstName);
    console.info("Last Name: %s", lastName);
    console.info("Postal Code %s", postalCode);
    console.info("Email address %s", email) ;
    res.status(200).end();
});

var portNumber = process.argv[2] || 3000;
console.info(process.argv[2]);

app.listen(parseInt(portNumber), function(){
   console.info("Webserver started on port 3000");
});